const gulp =require("gulp");
const sass =require("gulp-sass");
const image =require("gulp-image");
const concat =require("gulp-concat");
const browserSync =require("browser-sync");
gulp.task("html", function() {
    gulp.src("./src/**/*.html")
    .pipe(gulp.dest("dist/"));
});

gulp.task("sass",function() {
    return gulp.src("./src/**/*.scss")
    .pipe(sass().on("error", sass.logError))
    .pipe(concat("style.css"))
    .pipe (gulp.dest("./dist/styles"));
});

gulp.task("reloader",function(){
    browserSync({
        server:{
            baseDir:"./dist/"
        }
    });
});

gulp.task("watch",["sass","html","reloader"],function(){
    gulp.watch("./src/**/*.html",["html"]);
    gulp.watch("./dist/**/*.html",browserSync.reload);
    gulp.watch("./src/**/*.scss",["sass"]);
    gulp.watch("./dist/styles/**/*.css") .on("change",browserSync.reload);
});
   
